package timur.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import timur.Email;

@Controller
@EnableJms
public class SenderController {

    @RequestMapping(value = "/send")
    public void sendToJMS() {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("jmsTemplate");
        jmsTemplate.convertAndSend("Hello");
    }
}